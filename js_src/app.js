//
// Carousel Slide code that switches active on click.
//

const carousel = () => {
  $(".carousel-option").on("click", function() {
    if ($(".carousel-option").hasClass("active"))
      $(".carousel-option").removeClass("active");

    $(this).addClass("active");
  });
};

//
// Email Newsletter save
//

const emailNewsletter = () => {
  let newsletter = document.getElementById("email");

  newsletter.addEventListener("submit", function(action) {
    action.preventDefault();

    let email = {
      email: document.getElementById("emailInput").value
    };

    let email_serialized = JSON.stringify(email);

    http.open("POST", "/data/email.json");
    http.setRequestHeader(
      "Content-Type",
      "application/x-www-form-urlencoded;charset=UTF-8"
    );
    http.send(email_serialized);
  });
};

//
// Updating  Twitter Feed
//

const twitterFeed = () => {
  // http request

  http.open("GET", "/data/twitter.json", false);
  http.send(null);

  // Recieving data and creating array with necesssary html for each blogpost

  let i = 0;
  let tweets = [];
  let object = JSON.parse(http.responseText);
  object.forEach(obj => {
    tweets[i] = `
        <p> 
            <a href="#" class="highlight">@${obj.user}</a> 
            ${obj.text} 
        </p> 
        <h6 class="date">
            ${obj.date}
        </h6>
        `;
    i++;
  });

  // Getting individual tweet elements

  const tweetDiv = document.getElementsByClassName("tweet");
  i = 0;
  let ii = 0;

  // Interval function that slowly cycles through the content and repeats

  setInterval(function() {
    $(tweetDiv[i]).fadeOut("slow", () => {
      tweetDiv[i].innerHTML = tweets[ii];
    });

    $(tweetDiv[i]).fadeIn("slow", () => {
      i++;
      ii++;
      if (i > tweetDiv.length - 1) {
        i = 0;
      }
      if (ii > 8) {
        ii = 0;
      }
    });
  }, 5000);
};

//
// Blog News
//

const blogFeed = () => {
  // Http request

  http.open("GET", "/data/blogposts.json", false);
  http.send(null);

  // Recieving data and creating a array with the required html

  let i = 0;
  let blogposts = [];
  let object = JSON.parse(http.responseText);
  object.forEach(obj => {
    blogposts[i] = `
        <span class="datebox">
            <h6>${obj.date}</h6>
        </span>
        <div>
            <h4> ${obj.title} </h4>
            <p> ${obj.content} </p>
        </div>
        `;
    i++;
  });

  // Getting blogpost elements

  const blogDiv = document.getElementsByClassName("blogpost");
  i = 0;
  let ii = 0;

  // Interval function that slowly cycles through the content and repeats

  setInterval(function() {
    $(blogDiv[i]).fadeOut("slow", () => {
      blogDiv[i].innerHTML = blogposts[ii];
    });

    $(blogDiv[i]).fadeIn("slow", () => {
      i++;
      ii++;
      if (i > blogDiv.length - 1) {
        i = 0;
      }
      if (ii > 5) {
        ii = 0;
      }
    });
  }, 5000);
};

//
// Search function
//

const searchFunction = () => {
  // declaring our elements that we will workw ith

  const desktopSearch = document.getElementById("desktopSearch");
  const desktopInput = document.getElementById("searchInput");
  const dropdown = document.getElementById("searchDropdown");
  const mobileInput = document.getElementById("mobileSearch");
  const mobileDropdown = document.getElementById("mobileDropdown");

  // http request

  http.open("GET", "/data/products.json", false);
  http.send(null);

  // Creating the array with our products

  let i = 0;
  let products = [];
  let object = JSON.parse(http.responseText);
  object.forEach(obj => {
    products[i] = obj.name;
    i++;
  });

  // Desktop search

  desktopInput.addEventListener("keyup", () => {
    // Creating array with items that match the input value

    let searchedItem = desktopInput.value.toUpperCase();
    var found = [];
    products.forEach(obj => {
      if (obj.toUpperCase().indexOf(searchedItem) != -1) found.push(obj);
    });

    // Creating the HTML needed to show the found elements

    let HTMLconstruct = "";
    for (let ii = 0; ii <= 2; ii++) {
      if (found[ii] == undefined) break;
      HTMLconstruct +=
        '<a class="dropdown-item" href="#">' + found[ii] + "</a>";
    }
    dropdown.innerHTML = HTMLconstruct;

    // using Jquery so that the dropdown shows when you input something, and closes when the input is empty

    if (desktopInput.value == "") {
      desktopSearch.setAttribute("aria-expanded", "false");
      $(".desktopSearch").removeClass("show");
    } else {
      $(".desktopSearch").addClass("show");
      desktopSearch.setAttribute("aria-expanded", "true");
    }
  });

  // Mobile Search

  mobileInput.addEventListener("keyup", () => {
    // Creating array with items that match the input value

    let searchedItem = mobileInput.value.toUpperCase();
    var found = [];
    products.forEach(obj => {
      if (obj.toUpperCase().indexOf(searchedItem) != -1) found.push(obj);
    });

    // Creating the HTML needed to show the found elements

    let HTMLconstruct = "";
    for (let ii = 0; ii <= 2; ii++) {
      if (found[ii] == undefined) break;
      HTMLconstruct +=
        '<a class="dropdown-item" href="#">' + found[ii] + "</a>";
    }
    mobileDropdown.innerHTML = HTMLconstruct;
  });
};

//
// Proudct Grid
//

const productGrid = () => {
  let column = document.getElementById("column-grid");
  let row = document.getElementById("row-grid");

  row.addEventListener("click", () => {
    if (!$(".row.product").hasClass("column")) {
      $(".row.product").addClass("column");
      $(".description").show();
    }

    $("#column-grid").removeClass("active");
    $("#row-grid").addClass("active");
  });

  column.addEventListener("click", () => {
    if ($(".row.product").hasClass("column")) {
      $(".row.product").removeClass("column");
      $(".description").hide();
    }

    $("#column-grid").addClass("active");
    $("#row-grid").removeClass("active");
  });
};

//
// App
//

const http = new XMLHttpRequest();
function app() {
  carousel();
  emailNewsletter();
  twitterFeed();
  blogFeed();
  searchFunction();

  if (document.URL.indexOf("/products") != -1) productGrid();
}

app();
