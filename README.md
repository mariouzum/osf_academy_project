 **OSF ACADEMY FRONTEND PROJECT**
  **by Uzum Mario-Claudiu**

  Things used in this project:

  - HTML, CSS, Javascript
  - SCSS
  - Node Packet Manager (NPM)
  - GULP for compiling SCSS files into a compressed master.css
  - Custom Parts of Bootstrap v4 for certain elements
    