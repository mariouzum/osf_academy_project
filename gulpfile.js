"use strict";

var gulp = require("gulp");
var sass = require("gulp-sass");
var concat = require("gulp-concat");
var uglify = require("gulp-uglify-es").default;
var pump = require("pump");

sass.compiler = require("node-sass");

gulp.task("compress", function() {
  return gulp
    .src("./js_concat/master.js")
    .pipe(uglify())
    .pipe(gulp.dest("./js/"));
});

gulp.task("js:watch", function() {
  gulp.watch("./js_src/**/*.js", gulp.series(["concat", "compress"]));
});

gulp.task("concat", function() {
  return gulp
    .src([
      "./js_src/jquery.js",
      "./js_src/popper.js",
      "./js_src/bootstrap.min.js",
      "./js_src/app.js"
    ])
    .pipe(concat("master.js"))
    .pipe(gulp.dest("./js_concat/"));
});

gulp.task("sass", function() {
  return gulp
    .src("./scss/master.scss")
    .pipe(sass.sync({ outputStyle: "compressed" }).on("error", sass.logError))
    .pipe(gulp.dest("./css"));
});

gulp.task("sass:watch", function() {
  gulp.watch("./scss/**/*.scss", gulp.series(["sass"]));
});
